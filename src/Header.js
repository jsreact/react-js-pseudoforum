import React, { useEffect} from "react"

const Header = ({ setPosts,sortedPosts}) => {

    function searchPosts(event) {
        let query = event.target.value;
        let queriedPosts = [];
        //Search all posts
        sortedPosts.map(post => {
            Object.values(post).map(val=>{
                if(val == query){
                    queriedPosts.push(post);
                }
            })
        });
        populateSearchedPosts(queriedPosts);
    }

    /**
    * This function handles the likes and dislikes of a post.
    * @param {Object} posts : liked/disliked post
    * @param {object} event : event
    */
         function handleLikes(posts, event) {
            const fullPost = document.querySelector(`#${posts.topic_id}`);
            if (event.target.id == "like" || event.target.parentElement.id == "like") {
                posts.likes++;
                fullPost.children[2].children[2].innerText = `likes:${posts.likes}`;
            } else if (event.target.id === "dislike" || event.target.parentElement.id == "dislike") {
                posts.likes--;
                fullPost.children[2].children[2].innerText = `likes:${posts.likes}`;
            } else if (event.target.id === "delete" || event.target.parentElement.id == "delete") {
                let elem = document.querySelector(`#${posts.topic_id}`)
                elem.innerHTML = "";
                elem.getAttributeNames().map(attr => { elem.removeAttribute(attr) });
            }
        }

    function populateSearchedPosts(queriedPosts){
        let postsSection = document.querySelector(`#posts-section`);
        postsSection.innerHTML = "";
        queriedPosts.forEach(post => {
            /* Post section */
            const sectionPost = document.createElement(`section`);
            sectionPost.setAttribute(`key`,`${post.id}`);
            sectionPost.setAttribute(`id`,`${post.topic_id}`);
            sectionPost.setAttribute(`class`,`post`);
            postsSection.append(sectionPost);

            /* Post text */
            const parPost = document.createElement(`p`);
            parPost.innerText = `${post.text}`;
            sectionPost.append(parPost);

            /* Section images/icons */
            const sectionImages = document.createElement(`section`);
            sectionImages.setAttribute(`id`,`images`);
            //like
            const imageLike = document.createElement(`img`);
            imageLike.src = "icons/like.png"; imageLike.alt = "Like"; imageLike.setAttribute(`id`,`like`);
            sectionImages.append(imageLike);
            //dislike
            const imageDislike = document.createElement(`img`);
            imageDislike.src = "icons/dislike.png"; imageDislike.alt = "Dislike"; imageDislike.setAttribute(`id`,`dislike`);
            sectionImages.append(imageDislike);
            //delete
            const imageDelete = document.createElement(`img`);
            imageDelete.src = "icons/delete.png"; imageDelete.alt = "delete"; imageDelete.setAttribute(`id`,`delete`);
            sectionImages.append(imageDelete);
            sectionImages.addEventListener('click',(event)=>{handleLikes(post,event)})
            sectionPost.append(sectionImages);

            /* Post labels */
            const div = document.createElement(`div`);
            div.setAttribute(`class`,`post-labels`);
            div.innerHTML = `
                <label>by:${post.author}</label>
                <label>${post.date}</label>
                <label id="numberOfLikes">likes:${post.likes}</label>`;
            sectionPost.append(div);
        });
    }

    return (
        <header>
            <h2>Pseudo (mini) Forum</h2>
            <input id="search-bar" type="search" placeholder="Search... then press enter" onKeyUp={(event) => { if (event.keyCode == '13') { searchPosts(event) } }}></input>
        </header>
    );
}
export default Header;
