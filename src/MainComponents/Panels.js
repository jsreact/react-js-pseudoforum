import RankedTopics from "../PanelComponents/RankedTopics.js";
import RecentPosts from "../PanelComponents/RecentPosts.js";
import StatsPosts from "../PanelComponents/StatsPosts.js"

const Panels = ({sortedPosts,forum}) => {

 
    return ( 
        <section id="panels">
           <RankedTopics forum={forum} />
           <RecentPosts sortedPosts={sortedPosts}/>
           <StatsPosts />
        </section>
     );
}
 
export default Panels;