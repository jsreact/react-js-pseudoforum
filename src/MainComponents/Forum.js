import Category from "../ForumComponents/Category";
import Posts from "../ForumComponents/Posts";
import { useEffect, useState } from "react";
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Forum = ({forum,setForum,setCategory,setTopics,category,setPosts,handleLikes}) => {
   


    return (  
        <section id="forum">
            <Category forum={forum} setForum={setForum} setCategory={setCategory} 
                    category={category} setTopics={setTopics} />

            <Posts forum={forum} setPosts={setPosts} handleLikes={handleLikes}/>

        </section>
    );
}
 
export default Forum;