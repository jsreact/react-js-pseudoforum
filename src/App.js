import Header from './Header.js'
import Footer from './Footer.js'
import Main from './Main.js'
import { useEffect, useState } from "react";

function App() {

    let [forum, setForum] = useState([]);
    let [category, setCategory] = useState(null);
    let [topics, setTopics] = useState(null);
    let [post, setPosts] = useState(null);
    let allPostsArr = [];

    /**
    * This function handles the likes and dislikes of a post.
    * @param {Object} posts : liked/disliked post
    * @param {object} event : event
    */
    function handleLikes(posts, event) {
        if (event.target.id == "like" || event.target.parentElement.id == "like") {
            posts.likes++;
            setPosts(posts);
        } else if (event.target.id === "dislike" || event.target.parentElement.id == "dislike") {
            posts.likes--;
            setPosts(posts);
        } else if (event.target.id === "delete" || event.target.parentElement.id == "delete") {
            let elem = document.querySelector(`#${posts.topic_id}`)
            elem.innerHTML = "";
            elem.getAttributeNames().map(attr => { elem.removeAttribute(attr) });
        }
    }

    useEffect(() => {
            setCategory(document.querySelector(`#categories`));
    
            setTopics(document.querySelector(`#topics`));
    }, [category, topics]);

    useEffect(()=>{
        console.log(`fetching`)
        let url = "http://localhost:3001/categories";
        fetch(url).then(resp => {
            if (resp.ok) {
                return resp.json();
            }
            throw new Error("Failed to fetch data")

        }).then(resp => {
            setForum(resp);
            setTopics(document.querySelector(`#topics`));
            setCategory(document.querySelector(`#categories`));
            setPosts(document.querySelector(`#posts`));

        }).catch(err => {
            console.log(err);
        })
    },[])

    useEffect(()=>{
        setPosts();
    });

    /**
     * This function populates a list of posts.
     */
    function populateAllPosts() {
        forum.forEach((cat) => {
            cat.topicList.forEach(top => {
                top.listPosts.map(post => {
                    allPostsArr.push(post);
                })
            });
        })
    }




    populateAllPosts();
    let sortedPosts = allPostsArr.sort((a, b) => { return Date.parse(b.date) - Date.parse(a.date) });

    return (
        <div className="App">
            <Header forum={forum} sortedPosts={sortedPosts} setPosts={setPosts}/>
            <Main sortedPosts={sortedPosts} forum={forum} handleLikes={handleLikes} setForum={setForum} setCategory={setCategory}
                setTopics={setTopics} category={category} setPosts={setPosts} />
            <Footer />
        </div>
    );
}

export default App;
