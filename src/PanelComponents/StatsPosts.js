import { useEffect, useState } from "react";

const StatsPosts = () => {
    let [users,setUsers] = useState(null);

   useEffect(()=>{
   let urlUsers = "http://localhost:3002/users"; 

       fetch(urlUsers).then(resp =>{
           if(resp.ok){
               return resp.json();
           }
           throw new Error("Failed to fetch data")
           
       }).then(resp =>{
           setUsers(resp);
       }).catch(err =>{
           console.log(err);
       })
       },[]);

    return (  
        <section id="stats-posts">
            <h2 id="sp-title">Stats Posts</h2>
           <table>
               <tbody>
                <tr><th>Name</th><th>Posts</th></tr>
                    {users && users.map((elem=>{
                        return(
                        <tr key={elem.user_id}><td>{elem.user_id}</td><td>{elem.nberPosts}</td></tr>
                        )
                    }))}
                </tbody>
           </table>
        </section>
    );
}
 
export default StatsPosts;