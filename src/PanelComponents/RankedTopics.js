
const RankedTopics = ({forum}) => {
    let topicList = [];
    let ArrayOfTopics =  forum.forEach((elem)=>{ elem.topicList.map((topic)=>{  topicList.push(topic)});});
    let sortedArrayOfTopics = topicList.sort((a,b) =>{ return b.nberPost - a.nberPost; })

    return (
        <section id="ranked-topics">
            <h2 id="rt-title">Ranked Topics</h2>
            <table>
                <tbody>
                    <tr><th>Title</th><th>Number of Posts</th><th>Status</th></tr>
                    {sortedArrayOfTopics.map((t)=>{
                        return <tr key={t.topic_title}><td>{t.topic_title}</td><td>{t.nberPost} </td><td> {t.status}</td></tr>
                    })} 
                </tbody>
            </table>
        </section>
    );
}

export default RankedTopics;



