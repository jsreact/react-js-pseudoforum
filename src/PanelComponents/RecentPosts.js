
const RecentPosts = ({sortedPosts}) => {

    
    function differenceDate(date){
        const today = new Date();
        const todayTime = Date.parse(today);
        
        let difference =  Math.abs(Date.parse(date) -  Date.parse(today));
        let closestDay = Math.floor(difference/1000/60/60/24);
        if(closestDay < 1){
            return(`${Math.floor(difference/1000/60/60)} hours ago`)
        }
        if(closestDay > 365){
           
            return(`${Math.round(closestDay/465)} years ago`)
        }
        return (`${closestDay} days ago`);
    }

    return (  
        <section id="recent-posts">
            <h2 id="rp-title">Recent Posts</h2>
           <table>
               <tbody>
                    <tr><th>author</th><th>Rank</th><th>Time</th></tr>
                    {sortedPosts.map(post=>{
                        return <tr key={post.topic_id}><td>{post.author}</td><td>{post.rate}</td><td>{differenceDate(post.date)}</td></tr>
                    })}
             </tbody>
           </table>
        </section>
    );
}
 
export default RecentPosts;




