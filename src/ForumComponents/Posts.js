import { useEffect } from "react";
import Add from "./Add";
import { faThumbsUp } from "@fortawesome/free-solid-svg-icons";
import { faThumbsDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBan } from "@fortawesome/free-solid-svg-icons";


const Posts = ({forum,setPosts,handleLikes}) => {


    return (
        <section id="posts">
            <section id="posts-section" >
                {forum && forum.map(cat => {
                    return cat.topicList.map(topic => {
                        if (topic.topic_title === document.querySelector(`#topics`).value) {
                            return topic.listPosts.map(post => (
                                <section className='post' key={post.id} id={post.topic_id}>
                                    <p> {post.text} </p>

                                    <section id="images" onClick={(event)=>handleLikes(post,event)}>
                                        <FontAwesomeIcon icon={faThumbsUp} id="like"  />
                                        <FontAwesomeIcon icon={faThumbsDown} id="dislike" />
                                        <FontAwesomeIcon icon={faBan} id="delete"  />
                                    </section>

                                    <div className="post-labels">
                                        <label>by:{post.author}</label>
                                        <label>{post.date}</label>
                                        <label id="numberOfLikes">likes:{post.likes}</label>
                                    </div>
                                </section>
                            ))

                        }
                    })
                })}
            </section>
            <Add />
        </section>
    )
}

export default Posts;
