const Add = () => {
    return ( 
        <section id="new-post">
            <textarea type="text" id="post-text" />
            <button type="submit" id="submit-post">Submit Post</button> 
        </section>
     )
}
 
export default Add;