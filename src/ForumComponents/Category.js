import { render } from "@testing-library/react";
import ReactDOM from 'react-dom';
import { useEffect, useRef, useState } from "react";

 const Category = ({forum,setCategory,category,setTopics,setForum}) => {
     

/**
 * This function updates the topics when category is changed.
 */
function changeTopics(){
    setTopics(document.querySelector(`#related-topics`));
}

/**
 * This functiom update the topics when topics is changed.
 */
function changePosts() {
    setTopics(document.querySelector(`#related-topics`));
}

 return ( 
        <section id="categories-section">
            <div id="categories-div">
                <label>Category</label>
                
                    <select id="categories" onChange={()=>{changeTopics();}}>
                        {forum && forum.map(elem=>{
                            return <option id={elem.id} value={elem.name} key={elem.id}> {elem.name}  </option>
                        })}
                       
                    </select>
            </div>
            <div id="related-topics">
                <label>Related Topics</label>
                <select id="topics" onChange={()=>{changePosts();}}>
                        {category && forum.map(cat=>{
                            if(cat.name === document.querySelector(`#categories`).value){
                                return cat.topicList.map(topics=>{
                                    return <option id={topics.id} key={topics.id}> {topics.topic_title}  </option>
                                })
                            }
                        })}
                    </select>
            </div>
        </section>
    )
}
 

export default Category;

