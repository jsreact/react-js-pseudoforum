import Forum from "./MainComponents/Forum.js"
import Panels from "./MainComponents/Panels.js"
import { useEffect, useState } from "react";


const Main = ({forum,category,setForum,setCategory,setTopics,setPosts,sortedPosts,handleLikes}) => {


    return (  
        <section id="main-content">
            <Forum forum={forum} setCategory={setCategory} setTopics={setTopics} 
                    setForum={setForum} category={category} handleLikes={handleLikes} 
                    setPosts={setPosts} />
            <Panels forum={forum} sortedPosts={sortedPosts}/>
        </section>
    );
}
 
export default Main;