# Web-Development-II-React-project

## Description 
<p>
This project was done during the 2nd year, 3rd semester at Dawson College. This project presents all the skils we have learned during the course <b>420-320-DW Web Development II</b>. We have learned React Frmework in the last two weeks of the semester, hence this project is a representation of our collective skills.
This project is a pseudo forum where you can switch,look for statistics and search for different posts.
While you search for a post, you won't be allowed to directly delete the post nor like or dislike.
You  will only be  allowed to like and dislike and delete only if ur at that posts category and topic.
</p>

## Challenges 
<p>
We have faced a lot of challenges like deleting and updating a post. Liking and disliking a post.
We encountered a bug where there was no solution hence we had to overcome it with code adjustments and critical thinking.
Overall, this project was a bit harder than our usual projects due to the fact that we aren't used to using the framework React.js.
</p>

## Setup 
<p>
1. Make sure to have react modules installed with node.js in the folder structure "npx create-react-app finalproject_webdev_ii".
2. First put in the comppand line "npm run start" on where the folder is started. <br>
3. Than run "npx json-server --watch src/data/forum.json --port 3001".<br>
4. npx "json-server --watch src/data/users.json --port 3002"<br>
5. Enjoy!<br>

## Bugs 

- [X] Memory leak Error. Can't siwtch categories or topics after searching. Problem was caused by REACT ITSELF.

## Future Updates
- [X] Fix Search<br>
- [X] Add delete,like,dislike icons when query searched<br>
- [ ] Make submit post <br>

## Authors 
<h3> Davit Voskerchyan </h3>
- [Profile](https://gitlab.com/Davit_V "Davit Voskerchyan")
- [Email](mailto:davitvoskerchyan@gmail.com?subject=3rd Semester Final Java Project "3rd Semester Final Java Project")
- Has done the backend/logic.

<h3> Raphaël Canciani </h3>
- [Profile](https://gitlab.com/RaphyBoy "Raphaël Canciani")
- [Email](mailto:raphaelcanciani@gmail.com?subject=3rd Semester Final Java Project "3rd Semester Final Java Project")
- Has done the frontend/css.

<h3> Liscense </h3>
<p> <a href="https://choosealicense.com/licenses/gpl-3.0/"> GNU General Public License v3.0 </a> <p>
